// INPUTSCN.C (c) James Payne, 1999. 


#include <stdlib.h>
#include <stdio.h>

#include <string.h>

#include "char.h"

int readscn(char *name, char_def *cdef, skins *scn)
{
/* if no word is in progress:
    on emotion setting - change to a random base state from that emotion,
                         store emotion state
    if blink and blinkfreq are set, blink occasionally
    */


/* otherwise -
    change to half-open on word - start
    change to random full-open on following frame
    change to random full-open on sylable boundries
     - not the one we're already on
    change to half-open on word - end
    change to full closed the following frame
    */

FILE *infile;
int emo_num = 0;
int l;
int c;
int i,p;
char line[128];
int time;
char *bp;
int c_skin;
int k;
int m;
int bl_start = 0;
int bl_temp = 0;

int start,end;

infile = fopen(name,"rt");

scn->skin[0].num = cdef->emotion[0].base[0];
scn->skin[0].time = 0;

scn->changes = 1;

scn->entitynum = cdef->entitynum;

fgets(line,128,infile);
upper(line);

while (memcmp(line,"END",3))
{
if (isalpha(line[0])) {
 l = 0;
 while (isalpha(line[l])) l++;
 for (c = 0; c < cdef->numemotions; c++)
 {
  p = 0;
  for (i = 0; i < l; i++)
  {
   if (line[i] == cdef->emotion[c].emotionname[i])
   {
   p++;
   }
  }
   if (p == l) {
    emo_num = c;
    time = strtol(line+l,&bp,0);

//    printf("%i, %i",c,time);

    scn->skin[scn->changes].num = cdef->emotion[emo_num].base[0];
    scn->skin[scn->changes].time = time;

    scn->changes ++;

    }
 }
 }

if (!memcmp(line,"{",1))
 {
   time = strtol(line+1,&bp,0);
if (cdef->emotion[emo_num].blinkfreq > 0) {
   while (bl_start < time-1)
   {
    bl_temp = rand() % cdef->emotion[emo_num].blinkfreq;
    if (bl_temp == 0) {
      scn->skin[scn->changes].num = cdef->emotion[emo_num].blink[rand() % (cdef->emotion[emo_num].numblink)];
      scn->skin[scn->changes].time = bl_start;
      scn->changes ++;
      scn->skin[scn->changes].num = cdef->emotion[emo_num].base[rand() % (cdef->emotion[emo_num].numbase )];
      scn->skin[scn->changes].time = bl_start+1;
      scn->changes ++;

     }
    bl_start = bl_start + 5;
   }
        }
   scn->skin[scn->changes].num = cdef->emotion[emo_num].open[rand() % (cdef->emotion[emo_num].numopen)];
   scn->skin[scn->changes].time = time;
   start = time;

   scn->changes ++;
 }

if (!memcmp(line,"START DELAY",11))
 {
   time = strtol(line+1,&bp,0);
   scn->delay = strtol(line+11,&bp,0);
 }


if (!memcmp(line,"}",1))
 {

   time = strtol(line+1,&bp,0);
   k = start;
   while(k < time)
    {
    m = rand() % 100;
    if (m < 50) k = k+2;
    if ((m>=50) && (m < 80)) k = k+3;
    if ((m >=80) && (m < 90)) k = k+1;
    if (m>=90) k = k+4;
    if (k < time)
     {
      scn->skin[scn->changes].num = cdef->emotion[emo_num].open[rand() % (cdef->emotion[emo_num].numopen)];
      scn->skin[scn->changes].time = k;
      scn->changes ++;
     }
    };

   time; /* back to base frame */

   scn->skin[scn->changes].num = cdef->emotion[emo_num].base[rand() % (cdef->emotion[emo_num].numbase )];
   scn->skin[scn->changes].time = time;
   scn->changes ++;
   bl_start = time;
 }

if (!memcmp(line,"-",1))
 {
   time = strtol(line+1,&bp,0);
   scn->skin[scn->changes].num = cdef->emotion[emo_num].open[rand() % (cdef->emotion[emo_num].numopen)];
   if (cdef->emotion[emo_num].numopen > 1)
   {
   while(scn->skin[scn->changes].num == scn->skin[scn->changes-1].num)
   {
   scn->skin[scn->changes].num = cdef->emotion[emo_num].open[rand() % (cdef->emotion[emo_num].numopen)];
   }
   }

   scn->skin[scn->changes].time = time;
   scn->changes ++;
 }



fgets(line,128,infile);
upper(line);
}

for ( i = 0; i < scn->changes; i++)
{
//printf("%i : %i\n", scn->skin[i].num, scn->skin[i].time);
}



}
