# Lippy

## A Quake 2 lip-synching tool by Strange Company

This archive contains the original release source code for Lippy. The original
readme is preserved intact as readme.txt

Lippy is a utilty for adding lip synching to quake 2 .dm2 files. It was developed by Strange Company to allow the easy creation of a lip synched version of Eschaton: Nightfall.

## Credits

Credits

Lippy lead programmer: James Payne
Assistant programmer: Hugh Hancock
Bug fixes: Hugh Hancock
QA: Gordon McDonald

Lippy copyright 1999 Strange Company.
http://www.strangecompany.org/
email: nomad@strangecompany.org

This source code is licensed under [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International](http://creativecommons.org/licenses/by-nc-sa/4.0/)
