FILE *demo;
FILE *newdemo;

typedef float vec_t;

typedef vec_t vec3_t[3];

int ReadChar();
int ReadByte();
int ReadShort();
int ReadLong();
float ReadFloat();
char* ReadString();
vec_t ReadCoord();
vec_t ReadAngle();
vec_t ReadAngle16();
void ReadDir(vec_t * pos);
void ReadPosition(vec_t * pos);

unsigned char get_next_unsigned_char();
signed char get_next_signed_char();
short get_next_short();
long get_next_long();
float get_next_float();

void WriteChar(int a);
void WriteByte(int a);
void WriteShort(int a);
void WriteLong(long a);
void WriteFloat(float a);
void WriteString(char *a);
void WriteCoord(vec_t pos);
void WriteAngle(vec_t pos);
void WriteAngle16(float pos);
void WriteDir(vec_t * pos);
void WritePosition(vec_t * pos);

void put_next_unsigned_char(unsigned char a);
void put_next_signed_char(signed char a);
void put_next_short(short a);
void put_next_long(long a);
void put_next_float(float a);
