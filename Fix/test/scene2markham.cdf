entity 1

emotion normal
{
base 35
blink 39
freqblink 4
open 36
open 37
open 38
}

emotion angry
{
base 20
blink 24
freqblink 4
open 21
open 22
open 23
}

emotion shouting
{
base 20
blink 24
freqblink 4
open 40
open 41
open 42
}

emotion caring
{
base 44
blink 28
freqblink 4
open 26
open 27
open 27
}

emotion horror
{
base 30
blink 34
freqblink 4
open 31
open 32
open 33
}

emotion thinking
{
base 44
blink 48
freqblink 4
open 45
open 46
open 47
}

emotion smile
{
base 43
}

emotion extrashouting
{
base 50
blink 53
freqblink 4
open 54
open 55
open 56
}
end