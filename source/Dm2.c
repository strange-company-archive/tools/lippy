// DM2.C (c) James Payne, 1999.
// - portions (c) Uwe Girlich


/* parse .dm2 file */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "auxil.h"
#include "dm2.h"
#include "fly.h"

long tmodelindex;

void read_id0x03()
{
long entitytype;
long serverversion;
long dest_entity;
long entity;
long nextid;
long count;
vec3_t start;
vec3_t dest_origin;
long type;
vec3_t origin;
long flash_entity;
vec3_t trace_endpos;
vec3_t pos1, pos2, pos3, pos4;
vec3_t movedir;
long style;
long plat2flags;
long wait;

entitytype = ReadByte();
switch (entitytype) {
  // version problems
  // case TE_PLASMATRAIL:
  case TE_GREENBLOOD_new:
    if (serverversion >= 32) // game version >= 3.15
      goto impact_entity;
    else
      goto line_entity;
  break;
  // case TE_GREENBLOOD_old:
  case TE_BLUEHYPERBLASTER:
    if (serverversion >= 32) // game version >= 3.15
      goto line_entity;
    else
      goto impact_entity;
  break;
  // point entity
  case TE_EXPLOSION1:
  case TE_EXPLOSION2:
  case TE_ROCKET_EXPLOSION:
  case TE_GRENADE_EXPLOSION:
  case TE_ROCKET_EXPLOSION_WATER:
  case TE_GRENADE_EXPLOSION_WATER:
  case TE_BFG_EXPLOSION:
  case TE_BFG_BIGEXPLOSION:
  case TE_BOSSTPORT:
  case TE_PLASMA_EXPLOSION:
  case TE_PLAIN_EXPLOSION:
  case TE_CHAINFIST_SMOKE:
  case TE_TRACKER_EXPLOSION:
  case TE_TELEPORT_EFFECT:
  case TE_DBALL_GOAL:
  case TE_NUKEBLAST:
  case TE_WIDOWSPLASH:
  case TE_EXPLOSION1_BIG:
  case TE_EXPLOSION1_NP:
    ReadPosition(origin);
  break;
  // impact entity
  case TE_GUNSHOT:
  case TE_BLOOD:
  case TE_BLASTER:
  case TE_SHOTGUN:
  case TE_SPARKS:
  case TE_SCREEN_SPARKS:
  case TE_SHIELD_SPARKS:
  case TE_BULLET_SPARKS:
  // case TE_GREENBLOOD_new:
  // case TE_GREENBLOOD_old:
   case TE_BLASTER2:
 case TE_MOREBLOOD:
  case TE_HEATBEAM_SPARKS:
  case TE_HEATBEAM_STEAM:
  case TE_ELECTRIC_SPARKS:
  case TE_FLECHETTE:
  impact_entity:
    ReadPosition(origin);
    ReadDir(movedir);
  break;
  // line entity
  case TE_RAILTRAIL:
  case TE_BUBBLETRAIL:
  case TE_BFG_LASER:
  // case TE_PLASMATRAIL:
  // case TE_BLUEHYPERBLASTER:
  case TE_DEBUGTRAIL:
  case TE_BUBBLETRAIL2:
  line_entity:
    ReadPosition(origin);
    ReadPosition(trace_endpos);
  break;
  // special entity
  case TE_SPLASH:
  case TE_LASER_SPARKS:
  case TE_WELDING_SPARKS:
  case TE_TUNNEL_SPARKS:
    count = ReadByte();
    ReadPosition(origin);
    ReadDir(movedir);
    style = ReadByte();
  break;
  case TE_PARASITE_ATTACK:
  case TE_MEDIC_CABLE_ATTACK:
  case TE_HEATBEAM:
  case TE_MONSTER_HEATBEAM:
    entity = ReadShort();
    ReadPosition(origin);
    ReadPosition(trace_endpos);
  break;
  case TE_GRAPPLE_CABLE:
    entity = ReadShort();
    ReadPosition(origin);
    ReadPosition(trace_endpos);
    ReadPosition(pos1);
  break;
  case TE_FLAME: // Quake2 can't parse this!
    entity = ReadShort();
    count = ReadShort();
    ReadPosition(start);
    ReadPosition(origin);
    ReadPosition(pos1);
    ReadPosition(pos2);
    ReadPosition(pos3);
    ReadPosition(pos4);
  break;
  case TE_LIGHTNING:
    dest_entity = ReadShort();
    entity = ReadShort();
    ReadPosition(dest_origin);
    ReadPosition(origin);
  break;
  case TE_FLASHLIGHT:
    ReadPosition(origin);
    flash_entity = ReadShort();
  break;
  case TE_FORCEWALL:
    ReadPosition(origin);
    ReadPosition(trace_endpos);
    style = ReadShort();
  break;
  case TE_STEAM:
    nextid = ReadShort();
    count = ReadByte();
    ReadPosition(origin);
    ReadDir(movedir);
    style = ReadByte();
    plat2flags = ReadShort();
    if (nextid != -1)
      wait = ReadLong();
  break;
  case TE_WIDOWBEAMOUT:
   type = ReadShort();
    ReadPosition(origin);
  break;
  case TE_RAILTRAIL2: // senseless, I know
  default:

  break;
}

}

void read_id0x09()
{
long mask;
long soundnum;
float vol;
float attenuation;
#define ATTN_NONE 0
#define ATTN_NORM 1
#define ATTN_IDLE 2
#define ATTN_STATIC 3
float timeofs;
long channel;
#define CHAN_AUTO 0
#define CHAN_WEAPON 1
#define CHAN_VOICE 2
#define CHAN_ITEM 3
#define CHAN_BODY 4
long entity;
#define MAX_EDICTS 1024
vec3_t origin;

long entity_channel; // combined variable
mask = ReadByte();
soundnum = ReadByte();
vol = (mask & 0x01) ? ((float)ReadByte() / 255.0) : (1.0);
attenuation = (mask & 0x02) ? ((float)ReadByte() / 64.0) : (1.0);
timeofs = (mask & 0x10) ? ((float)ReadByte() * 0.001) : (0.0);
if (mask & 0x08) {
entity_channel = ReadShort();
entity = (entity_channel >> 3);
channel = entity_channel & 0x07;
if (entity > MAX_EDICTS) {
/* error("CL_ParseStartSoundPacket: ent = %i", entity); */
}
} else {
channel = 0;
entity = 0;
}
if (mask & 0x04) {
ReadPosition(origin);
}

}

void read_id0x0e()
{
  long mask;
  long entity;
  vec3_t origin;
  vec3_t angles;
  vec3_t old_origin;
  long modelindex;
  long modelindex2;
  long modelindex3;
  long modelindex4;
  long frame;
  long skin;
  long vwep;
  long effects;
  long renderfx;
  long solid;
  long sound;
  long event;
       typedef enum
       {
               EV_NONE,             // 0
               EV_ITEM_RESPAWN,     // 1
               EV_FOOTSTEP,         // 2
               EV_FALLSHORT,        // 3
               EV_FALL,             // 4
               EV_FALLFAR,          // 5
               EV_PLAYER_TELEPORT   // 6
       } entity_event_t;
  mask = ReadByte();
  if (mask & 0x00000080) mask |= (ReadByte() <<  8);
  if (mask & 0x00008000) mask |= (ReadByte() << 16);
  if (mask & 0x00800000) mask |= (ReadByte() << 24);
  entity = (mask & 0x00000100) ? ReadShort() : ReadByte();
  if (mask & 0x00000800) modelindex = ReadByte();
  if (mask & 0x00100000) modelindex2 = ReadByte();
  if (mask & 0x00200000) modelindex3 = ReadByte();
  if (mask & 0x00400000) modelindex4 = ReadByte();
  if (mask & 0x00000010) frame = ReadByte();
  if (mask & 0x00020000) frame = ReadShort();
  if (mask & 0x00010000) {
    if (mask & 0x02000000) skin = ReadLong();
    else skin = ReadByte();
  }
  else {
    if (mask & 0x02000000) skin = ReadShort();
  }
  vwep = skin >> 8;
  skin &= 0xFF;
  if (mask & 0x00004000) {
    if (mask & 0x00080000) effects = ReadLong();
    else effects = ReadByte();
  }
  else {
    if (mask & 0x00080000) effects = ReadShort();
  }
  if (mask & 0x00001000) {
    if (mask & 0x00040000) renderfx = ReadLong();
    else renderfx = ReadByte();
  }
  else {
    if (mask & 0x00040000) renderfx = ReadShort();
  }
  if (mask & 0x00000001) origin[0] = ReadCoord();
  if (mask & 0x00000002) origin[1] = ReadCoord();
  if (mask & 0x00000200) origin[2] = ReadCoord();
  if (mask & 0x00000400) angles[0] = ReadAngle();
  if (mask & 0x00000004) angles[1] = ReadAngle();
  if (mask & 0x00000008) angles[2] = ReadAngle();
  if (mask & 0x01000000) ReadPosition(old_origin);
  if (mask & 0x04000000) sound = ReadByte();
  event = (mask & 0x00000020) ? ReadByte() : 0;
  if (mask & 0x08000000) solid = ReadShort();
}

void read_id0x11()
{
  int i;
  long mask;
  long mask2;
  long pm_type;
  vec3_t origin;
  vec3_t velocity;
  char pm_flags;
  char teleport_time;
  char pm_time;
  short gravity;
  vec3_t delta_angles;
  vec3_t viewangles;
  vec3_t viewoffset;
  vec3_t kick_angles;
  vec3_t gunangles;
  vec3_t gunoffset;
  int gunindex;
  int gunframe;
  float blend[4];
  long fov;
  int rdflags;
  #define MAX_STATS 32

  short stats[MAX_STATS];
     #define STAT_HEALTH_ICON 0
     #define STAT_HEALTH 1
     #define STAT_AMMO_ICON 2
     #define STAT_AMMO 3
     #define STAT_ARMOR_ICON 4
     #define STAT_ARMOR 5
     #define STAT_SELECTED_ICON 6
     #define STAT_PICKUP_ICON 7
     #define STAT_PICKUP_STRING 8
     #define STAT_TIMER_ICON 9
     #define STAT_TIMER 10
     #define STAT_HELPICON 11
     #define STAT_SELECTED_ITEM 12
     #define STAT_LAYOUTS 13
     #define STAT_FRAGS 14
     #define STAT_FLASHES 15
     #define STAT_CHASE 16
     #define STAT_SPECTATOR 17

     mask = ReadShort();
     if (mask & 0x0001) pm_type = ReadByte();
     if (mask & 0x0002)
       ReadPosition(origin);
     if (mask & 0x0004)
       ReadPosition(velocity);
     if (mask & 0x0008) teleport_time = ReadByte();
     if (mask & 0x0010) pm_flags = ReadByte();
     if (mask & 0x0020) gravity = ReadShort();
     if (mask & 0x0040) {
       delta_angles[0] = ReadAngle16();
       delta_angles[1] = ReadAngle16();
       delta_angles[2] = ReadAngle16();
     }
     if (mask & 0x0080) {
       viewoffset[0] = ReadChar() / 4.0;
       viewoffset[1] = ReadChar() / 4.0;
       viewoffset[2] = ReadChar() / 4.0;
     }
     if (mask & 0x0100) {
       viewangles[0] = ReadAngle16();
       viewangles[1] = ReadAngle16();
       viewangles[2] = ReadAngle16();
     }
     if (mask & 0x0200) {
       kick_angles[0] = ReadChar() / 4.0;
       kick_angles[1] = ReadChar() / 4.0;
       kick_angles[2] = ReadChar() / 4.0;
     }
     if (mask & 0x1000) gunindex = ReadByte();
     if (mask & 0x2000) {
       gunframe = ReadByte();
       gunoffset[0] = ReadChar() / 4.0;
       gunoffset[1] = ReadChar() / 4.0;
       gunoffset[2] = ReadChar() / 4.0;
       gunangles[0] = ReadChar() / 4.0;
       gunangles[1] = ReadChar() / 4.0;
       gunangles[2] = ReadChar() / 4.0;
     }
     if (mask & 0x0400) {
       blend[0] = ReadByte() / 255.0;
       blend[1] = ReadByte() / 255.0;
       blend[2] = ReadByte() / 255.0;
       blend[3] = ReadByte() / 255.0;
     }
     if (mask & 0x0800) fov = ReadByte();
     if (mask & 0x4000) rdflags = ReadByte();
     mask2 = ReadLong();
     for (i=0;i<32;i++) if (mask2 & (0x00000001 << i)) stats[i] = ReadShort();

}

void rewrite_id0x11(campath_t * path, int num)
{
  int i;
  long mask;
  long mask2;
  long pm_type;
  vec3_t origin;
  vec3_t velocity;
  char pm_flags;
  char teleport_time;
  char pm_time;
  short gravity;
  vec3_t delta_angles;
  vec3_t viewangles;
  vec3_t viewoffset;
  vec3_t kick_angles;
  vec3_t gunangles;
  vec3_t gunoffset;
  int gunindex;
  int gunframe;
  float blend[4];
  long fov;
  int rdflags;
  #define MAX_STATS 32

  short stats[MAX_STATS];
     #define STAT_HEALTH_ICON 0
     #define STAT_HEALTH 1
     #define STAT_AMMO_ICON 2
     #define STAT_AMMO 3
     #define STAT_ARMOR_ICON 4
     #define STAT_ARMOR 5
     #define STAT_SELECTED_ICON 6
     #define STAT_PICKUP_ICON 7
     #define STAT_PICKUP_STRING 8
     #define STAT_TIMER_ICON 9
     #define STAT_TIMER 10
     #define STAT_HELPICON 11
     #define STAT_SELECTED_ITEM 12
     #define STAT_LAYOUTS 13
     #define STAT_FRAGS 14
     #define STAT_FLASHES 15
     #define STAT_CHASE 16
     #define STAT_SPECTATOR 17


     mask = ReadShort();
     if (mask & 0x0001) pm_type = ReadByte();
     if (mask & 0x0002)
       ReadPosition(origin);
     if (mask & 0x0004)
       ReadPosition(velocity);
     if (mask & 0x0008) teleport_time = ReadByte();
     if (mask & 0x0010) pm_flags = ReadByte();
     if (mask & 0x0020) gravity = ReadShort();
     if (mask & 0x0040) {
       delta_angles[0] = ReadAngle16();
       delta_angles[1] = ReadAngle16();
       delta_angles[2] = ReadAngle16();
     }
     if (mask & 0x0080) {
       viewoffset[0] = ReadChar() / 4.0;
       viewoffset[1] = ReadChar() / 4.0;
       viewoffset[2] = ReadChar() / 4.0;
     }
     if (mask & 0x0100) {
       viewangles[0] = ReadAngle16();
       viewangles[1] = ReadAngle16();
       viewangles[2] = ReadAngle16();
       }
     if (mask & 0x0200) {
       kick_angles[0] = ReadChar() / 4.0;
       kick_angles[1] = ReadChar() / 4.0;
       kick_angles[2] = ReadChar() / 4.0;
     }
     if (mask & 0x1000) gunindex = ReadByte();
     if (mask & 0x2000) {
       gunframe = ReadByte();
       gunoffset[0] = ReadChar() / 4.0;
       gunoffset[1] = ReadChar() / 4.0;
       gunoffset[2] = ReadChar() / 4.0;
       gunangles[0] = ReadChar() / 4.0;
       gunangles[1] = ReadChar() / 4.0;
       gunangles[2] = ReadChar() / 4.0;
     }
     if (mask & 0x0400) {
       blend[0] = ReadByte() / 255.0;
       blend[1] = ReadByte() / 255.0;
       blend[2] = ReadByte() / 255.0;
       blend[3] = ReadByte() / 255.0;
     }
     if (mask & 0x0800) fov = ReadByte();
     if (mask & 0x4000) rdflags = ReadByte();
     mask2 = ReadLong();
     for (i=0;i<32;i++) if (mask2 & (0x00000001 << i)) stats[i] = ReadShort();

/* done reading, now write with different values... */


     mask = mask | 0x0102;  /* fix origin and viewangles */
     WriteShort(mask);

     if (mask & 0x0001) WriteByte(pm_type);
     if (mask & 0x0002) {
       origin[0] = path->campos[num].x;
       origin[1] = path->campos[num].y;
       origin[2] = path->campos[num].z;
       WritePosition(origin);
       }
     if (mask & 0x0004)
       WritePosition(velocity);
     if (mask & 0x0008) WriteByte(teleport_time);
     if (mask & 0x0010) WriteByte(pm_flags);
     if (mask & 0x0020) WriteShort(gravity);
     if (mask & 0x0040) {
       delta_angles[0] = 0;
       delta_angles[1] = 0;
       delta_angles[2] = 0;

       WriteAngle16(delta_angles[0]);
       WriteAngle16(delta_angles[1]);
       WriteAngle16(delta_angles[2]);
     }
     if (mask & 0x0080) {
       WriteChar(viewoffset[0]*4);
       WriteChar(viewoffset[1]*4);
       WriteChar(viewoffset[2]*4);
     }

     if (mask & 0x0100) {
       viewangles[0] = path->camang[num].p;
       viewangles[1] = path->camang[num].y;
       viewangles[2] = path->camang[num].r;
       //printf("%i, ",(int)(path->camang[num].p*10));
       WriteAngle16(viewangles[0]);
       WriteAngle16(viewangles[1]);
       WriteAngle16(viewangles[2]);
     }

     if (mask & 0x0200) {
       kick_angles[0] = 0;
       kick_angles[1] = 0;
       kick_angles[2] = 0;

       WriteChar(kick_angles[0]*4);
       WriteChar(kick_angles[1]*4);
       WriteChar(kick_angles[2]*4);
     }
     if (mask & 0x1000) WriteByte(gunindex);
     if (mask & 0x2000) {
       WriteByte(gunframe);
       WriteChar(gunoffset[0]*4);
       WriteChar(gunoffset[1]*4);
       WriteChar(gunoffset[2]*4);
       WriteChar(gunangles[0]*4);
       WriteChar(gunangles[1]*4);
       WriteChar(gunangles[2]*4);
     }
     if (mask & 0x0400) {
       WriteByte(blend[0]*255.0);
       WriteByte(blend[1]*255.0);
       WriteByte(blend[2]*255.0);
       WriteByte(blend[3]*255.0);
     }
     if (mask & 0x0800) WriteByte(fov);
     if (mask & 0x4000) WriteByte(rdflags);
     WriteLong(mask2);
     for (i=0;i<32;i++) if (mask2 & (0x00000001 << i)) WriteShort(stats[i]);
}



void read_id0x12()
{
        long mask;
        long entity;
        long remove;
        vec3_t origin;
        vec3_t angles;
        vec3_t old_origin;
        long modelindex;
        long modelindex2;
        long modelindex3;
        long modelindex4;
        long frame;
        long skin;
        long vwep;
        long effects;
        long renderfx;
        long solid;
        long sound;
        long event;

     for (;;) {
       mask = ReadByte();
       if (mask & 0x00000080) mask |= (ReadByte() <<  8);
       if (mask & 0x00008000) mask |= (ReadByte() << 16);
       if (mask & 0x00800000) mask |= (ReadByte() << 24);
       entity = (mask & 0x00000100) ? ReadShort() : ReadByte();
/*       if (entity >= MAX_EDICTS) error("CL_ParsePacketEntities: bad number:%i",entity);*/
       if (entity == 0) break;
       remove = (mask & 0x00000040) ? 1 : 0;
       if (mask & 0x00000800) modelindex = ReadByte();
       if (mask & 0x00100000) modelindex2 = ReadByte();
       if (mask & 0x00200000) modelindex3 = ReadByte();
       if (mask & 0x00400000) modelindex4 = ReadByte();
       if (mask & 0x00000010) frame = ReadByte();
       if (mask & 0x00020000) frame = ReadShort();
       if (mask & 0x00010000) {
         if (mask & 0x02000000) skin = ReadLong();
         else skin = ReadByte();
       }
       else {
         if (mask & 0x02000000) skin = ReadShort();
       }
       vwep = skin >> 8;
       skin &= 0xFF;
       if (mask & 0x00004000) {
         if (mask & 0x00080000) effects = ReadLong();
         else effects = ReadByte();
       }
       else {
         if (mask & 0x00080000) effects = ReadShort();
       }
       if (mask & 0x00001000) {
         if (mask & 0x00040000) renderfx = ReadLong();
         else renderfx = ReadByte();
       }
       else {
         if (mask & 0x00040000) renderfx = ReadShort();
       }
       if (mask & 0x00000001) origin[0] = ReadCoord();
       if (mask & 0x00000002) origin[1] = ReadCoord();
       if (mask & 0x00000200) origin[2] = ReadCoord();
       if (mask & 0x00000400) angles[0] = ReadAngle();
       if (mask & 0x00000004) angles[1] = ReadAngle();
       if (mask & 0x00000008) angles[2] = ReadAngle();
       if (mask & 0x01000000) ReadPosition(old_origin);

       if (mask & 0x04000000) sound = ReadByte();
       event = (mask & 0x00000020) ? ReadByte() : 0;
       if (mask & 0x08000000) solid = ReadShort();
     }


}


int rewrite_id0x12(int new_skin, int new_entity)
{
        long mask;
        long entity;
        long remove;
        vec3_t origin;
        vec3_t angles;
        vec3_t old_origin;
        long modelindex;
        long modelindex2;
        long modelindex3;
        long modelindex4;
        long frame;
        long skin;
        long vwep;
        long effects;
        long renderfx;
        long solid;
        long sound;
		int bte;
        long event;
        int done = 0;
        long tmask;

     for (;;) {
       mask = ReadByte();
       if (mask & 0x00000080) mask |= (ReadByte() <<  8);
       if (mask & 0x00008000) mask |= (ReadByte() << 16);
       if (mask & 0x00800000) mask |= (ReadByte() << 24);
       entity = (mask & 0x00000100) ? ReadShort() : ReadByte();

// If there are packetentities, then run the next bit of code. 

       if (entity != 0 ) {
       if (entity == new_entity) done = 1;

//Where the hell does he use done?

       remove = (mask & 0x00000040) ? 1 : 0;
       if (mask & 0x00000800)
        {
        modelindex = ReadByte();
        if (entity == new_entity) tmodelindex = modelindex;
        }
       if (mask & 0x00100000) modelindex2 = ReadByte();
       if (mask & 0x00200000) modelindex3 = ReadByte();
       if (mask & 0x00400000) modelindex4 = ReadByte();
       if (mask & 0x00000010) frame = ReadByte();
       if (mask & 0x00020000) frame = ReadShort();
       if (mask & 0x00010000) {
         if (mask & 0x02000000) 
		 {
			 skin = ReadLong();
		 }
         else  skin = ReadByte();
       }
       else {
         if (mask & 0x02000000) skin = ReadShort();
       }
       vwep = skin >> 8;
       skin &= 0xFF;
       if (mask & 0x00004000) {
         if (mask & 0x00080000) effects = ReadLong();
         else effects = ReadByte();
       }
       else {
         if (mask & 0x00080000) effects = ReadShort();
       }
       if (mask & 0x00001000) {
         if (mask & 0x00040000) renderfx = ReadLong();
         else renderfx = ReadByte();
       }
       else {
         if (mask & 0x00040000) renderfx = ReadShort();
       }
       if (mask & 0x00000001) origin[0] = ReadCoord();
       if (mask & 0x00000002) origin[1] = ReadCoord();
       if (mask & 0x00000200) origin[2] = ReadCoord();
       if (mask & 0x00000400) angles[0] = ReadAngle();
       if (mask & 0x00000004) angles[1] = ReadAngle();
       if (mask & 0x00000008) angles[2] = ReadAngle();
       if (mask & 0x01000000) ReadPosition(old_origin);

       if (mask & 0x04000000) sound = ReadByte();
       event = (mask & 0x00000020) ? ReadByte() : 0;
       if (mask & 0x08000000) solid = ReadShort();
     }
// if entity did not equal zero, all the above steps are carried out. That reads a bunch of stuff in. 
     if ((entity == 0) && (done == 0)) {
     /* create new packetentity entry */
     tmask = 0x02010000;
     tmask = tmask | 0x00808080;
     tmask = tmask | 0x00000100;
     tmask = tmask | 0x00000800;
     WriteByte(tmask & 0xFF);
     if (tmask & 0x00000080) WriteByte((tmask >> 8) & 0xFF);
     if (tmask & 0x00008000) WriteByte((tmask >> 16) & 0xFF);
     if (tmask & 0x00800000) WriteByte((tmask >> 24) & 0xFF);

     if (tmask & 0x00000100) WriteShort(new_entity); else WriteByte(new_entity);
       if (tmask & 0x00000800) WriteByte(tmodelindex);


       if (tmask & 0x00010000) {
         if (tmask & 0x02000000) WriteLong(new_skin);
         else WriteByte(new_skin);
       }
       else {
         if (tmask & 0x02000000) WriteShort(new_skin);
       }
     }

//end of new entity.

       if (entity == new_entity) {
        mask = mask | 0x00010000;
        mask = mask | 0x00808080;
        skin = new_skin;
        }
       WriteByte(mask & 0xFF);
       if (mask & 0x00000080) WriteByte((mask >> 8) & 0xFF);
       if (mask & 0x00008000) WriteByte((mask >> 16) & 0xFF);
       if (mask & 0x00800000) WriteByte((mask >> 24) & 0xFF);

       if (mask & 0x00000100) WriteShort(entity); else WriteByte(entity);

       if (entity == 0) break;
//We added a new entity, so kill it there. 
//if (done == 1) printf ("%d , %d ", skin, entity);
//       remove = (mask & 0x00000040) ? 1 : 0;
       if (mask & 0x00000800) WriteByte(modelindex);
       if (mask & 0x00100000) WriteByte(modelindex2);
       if (mask & 0x00200000) WriteByte(modelindex3);
       if (mask & 0x00400000) WriteByte(modelindex4);
       if (mask & 0x00000010) WriteByte(frame);
       if (mask & 0x00020000) WriteShort(frame);
       skin = skin | (vwep << 8);
       if (mask & 0x00010000) {
         if (mask & 0x02000000) WriteLong(skin);
         else WriteByte(skin);
       }
       else {
         if (mask & 0x02000000) WriteShort(skin);
       }
//       vwep = skin >> 8;
//      skin &= 0xFF;
       if (mask & 0x00004000) {
         if (mask & 0x00080000) WriteLong(effects);
         else WriteByte(effects);
       }
       else {
         if (mask & 0x00080000) WriteShort(effects);
       }
       if (mask & 0x00001000) {
         if (mask & 0x00040000)  WriteLong(renderfx);
         else WriteByte(renderfx);
       }
       else {
         if (mask & 0x00040000) WriteShort(renderfx);
		 }
       if (mask & 0x00000001) WriteCoord(origin[0]);
       if (mask & 0x00000002) WriteCoord(origin[1]);
       if (mask & 0x00000200) WriteCoord(origin[2]);
       if (mask & 0x00000400) WriteAngle(angles[0]);
       if (mask & 0x00000004) WriteAngle(angles[1]);
       if (mask & 0x00000008) WriteAngle(angles[2]);
       if (mask & 0x01000000) WritePosition(old_origin);

       if (mask & 0x04000000) WriteByte(sound);

       if (mask & 0x00000020) WriteByte(event);
       if (mask & 0x08000000) WriteShort(solid);
     }
return done; 

}









void read_id0x14()
{
long uk_b1;
long h;
int count;
int i;
h = ReadLong();
ReadLong();
ReadByte();
count = ReadByte();
for (i=0;i<count;i++) ReadByte();
}

int read_message()
/* reads the next message in the file */
{
unsigned char ID;
int i;
int size;
char *str;

fread(&ID,sizeof(ID),1,demo);

/* printf("Message ID: %u\n",ID);*/

if (ID == 0x00) {
  // Error!
  }

if (ID == 0x01) {
  ReadShort(); ReadByte();
  }

if (ID == 0x02) {
  ReadShort(); ReadByte();
  }

if (ID == 0x03) {
  read_id0x03();
  }

if (ID == 0x04) {
  ReadString();
  }

if (ID == 0x05)
  {
  for (i=0 ; i<256 ; i++) ReadShort();
  }

if (ID == 0x06)
  {
  }

if (ID == 0x07)
  {
  }

if (ID == 0x08)
  {
  }

if (ID == 0x09)
  {
  read_id0x09();
  }

if (ID == 0x0A)
  {
  ReadByte();
  ReadString();
  }

if (ID == 0x0B)
  {
  ReadString();
  }

if (ID == 0x0C)
  {
  ReadLong();
  ReadLong();
  ReadByte();
str = ReadString();
/* printf("%s\n",str); */
  ReadShort();
str = ReadString();
/* printf("%s\n",str); */


  }

if (ID == 0x0D)
   {
   ReadShort();
   str = ReadString();
   }

if (ID == 0x0E)
   {
   read_id0x0e();
   }

if (ID == 0x0F)
   {
   ReadString();
   }

if (ID == 0x10)
  {
  size = ReadShort();
  ReadByte();
  ReadString();
  for ( i=0 ; i<size ; i++ ) ReadByte();

  }

if (ID == 0x11)
 {
 read_id0x11();
 }

if (ID == 0x12)
 {
 read_id0x12();
 }

if (ID == 0x13)
 {
   // unknown
 }

if (ID == 0x14)
 {
 read_id0x14();
  // Change for server recorded demos...
 }

return(ID);
}

long read_frame_num()
{
unsigned char ID;
fread(&ID,sizeof(ID),1,demo);
return ReadLong();

}



/*
int main()
{
int more;
int p;
fpos_t fp;
unsigned long l1,l2;
unsigned char c1,c2;
long block = 0;
long pos = 4;
char mess = 0;

demo = fopen("credits.dm2","rb");
while(block < 20) {
 block++;
 printf("Block: %i\n",block);
 fread(&l1,sizeof(l1),1,demo);
 printf(" - size: %u\n",l1);
 fread(&c1,sizeof(c1),1,demo);
 printf(" - messages: %U\n",c1);

 mess = 0;

 fseek(demo,pos,SEEK_SET);
 more = 1;
 while (more == 1) {
 mess++;

 read_message();
 fgetpos(demo,&fp);
 p = (int)fp;
 if (p>=pos+l1) more = 0;
 }


 fseek(demo,l1+pos,SEEK_SET);
 pos = pos + l1 + 4;
}
fclose(demo);
return EXIT_SUCCESS;
}

*/
