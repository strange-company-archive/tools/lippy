entity 2

emotion normal
{
base 13
half 13
blink 17
blinkfreq 12
open 14
open 15
open 16
}
emotion angry
{
base 3
half 3
blink 7
blinkfreq 20
open 4
open 5
open 6
}
emotion happy
{
base 8
half 8
blink 12
blinkfreq 7
open 9
open 10
open 11
}
emotion caring
{
base 30
half 30
blink 33
blinkfreq 30
open 31
open 32
}
emotion sad
{
base 18
half 18
blink 22
blinkfreq 5
open 19
open 20
open 21
}

end