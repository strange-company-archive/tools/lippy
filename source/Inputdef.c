// INPUTDEF.C (c) James Payne, 1999. 


/* read in a character definition file */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "char.h"


void upper(char *str)
{
 int i;
 for (i = 0; i<strlen(str); i++)
 {
  str[i] = toupper(str[i]);
 }
}


int readdef(char *name, char_def *cdef)
{
/* read each line */
/* depending on first characters, do something else with the rest of a line */

/* ENTITY - read the entity number */
/* DEFAULT - read a text string */

/* EMOTION - read a text string */
/* { - parse emotion details */
/* BASE, HALF, BLINK, BLINKFREQ, OPEN - add skinnumber definition to emotion */

FILE *infile;
char line[128];
char *bp;
int i;
int cnum;


cdef->numemotions = 0;
cdef->entitynum = 0;


infile = fopen(name,"rt");

fgets(line,128,infile);
upper(line);

while (memcmp(line,"END",3))
{
if (!memcmp(line,"ENTITY",6))
 {
  /* read a number from this line, write it to cdef->entity */
  cdef->entitynum = strtol(line+6,&bp,0);
 }
if (!memcmp(line,"EMOTION",7))
 {
  cnum = cdef->numemotions;

  /* read the second string from this line, write it to the emotion details */
  for (i = 0; i<32; i++)
  {
   cdef->emotion[cnum].emotionname[i] = 32;
  }
  for (i = 8; i < strlen(line)-1; i++)
  {
   cdef->emotion[cnum].emotionname[i-8] = line[i];
  }
  cdef->emotion[cnum].emotionname[31] = NULL;
  cdef->emotion[cnum].numbase = 0;
  cdef->emotion[cnum].numhalf = 0;
  cdef->emotion[cnum].numopen = 0;
  cdef->emotion[cnum].numblink = 0;
  cdef->emotion[cnum].blinkfreq = 0;

//  printf(":%s:\n",cdef->emotion[cnum].emotionname);
 }

if (!memcmp(line,"BASE",4))
 {
 cdef->emotion[cnum].base[cdef->emotion[cnum].numbase] = strtol(line+4,&bp,0);
 cdef->emotion[cnum].numbase ++;
 }
if (!memcmp(line,"BLINK",5))
 {
 cdef->emotion[cnum].blink[cdef->emotion[cnum].numblink] = strtol(line+5,&bp,0);
 cdef->emotion[cnum].numblink ++;

 }
if (!memcmp(line,"HALF",4))
 {
 cdef->emotion[cnum].half[cdef->emotion[cnum].numhalf] = strtol(line+4,&bp,0);
 cdef->emotion[cnum].numhalf ++;
 }
if (!memcmp(line,"OPEN",4))
 {
 cdef->emotion[cnum].open[cdef->emotion[cnum].numopen] = strtol(line+4,&bp,0);
// printf("open: %i\n",cdef->emotion[cnum].open[cdef->emotion[cnum].numopen]);
 cdef->emotion[cnum].numopen ++;

 }

if (!memcmp(line,"FREQBLINK",9))
 {
 cdef->emotion[cnum].blinkfreq = strtol(line+9,&bp,0);
 }

if (!memcmp(line,"}",1))
 {
 cdef->numemotions++;
 }


fgets(line,128,infile);
upper(line);
}

printf("Num: %i\n",cdef->numemotions);

}
