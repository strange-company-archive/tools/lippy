// MAIN.C (c) James Payne, 1999.

#include <stdlib.h>
#include <stdio.h>

#include "char.h"

int main(int argc, char *argv[])
{

 char_def character;
 skins * skn;


 int i;
 int v;
 int o;
 int perror = 1;
 char * indemofile = NULL;
 char * outdemofile = NULL;
 char * cdffile = NULL;
 char * sdffile = NULL;
 FILE *stream;
 skn = malloc(sizeof(skins));


printf("\n");
printf("--------------------------------------------------------------------------------");
printf("           - Lippy v0.01a Copyright (c) 1999, By James Payne- \n");
printf("                     - Lip-synching Tool, For QuakeII -\n");
printf("--------------------------------------------------------------------------------");
printf("\n");

printf("Reading parameters... ");
fflush(stdout);


if (argc == 1) {
 perror = 1;
 } else {
 for (i = 1; i < argc; i++) {
   if (memcmp(argv[i],"-s",2) == 0) {
   sdffile = (char *)malloc(strlen(argv[i])-1);
   memcpy(sdffile,argv[i] + 2, strlen(argv[i])-2);
   sdffile[strlen(argv[i])-2] = NULL;
   }
   if (memcmp(argv[i],"-c",2) == 0) {
   cdffile = (char *)malloc(strlen(argv[i])-1);
   memcpy(cdffile,argv[i] + 2, strlen(argv[i])-2);
   cdffile[strlen(argv[i])-2] = NULL;
   }
   if (memcmp(argv[i],"-i",2) == 0) {
   indemofile = (char *)malloc(strlen(argv[i])-1);
   memcpy(indemofile,argv[i] + 2, strlen(argv[i])-2);
   indemofile[strlen(argv[i])-2] = NULL;
   }
   if (memcmp(argv[i],"-o",2) == 0) {
   outdemofile = (char *)malloc(strlen(argv[i])-1);
   memcpy(outdemofile,argv[i] + 2, strlen(argv[i])-2);
   outdemofile[strlen(argv[i])-2] = NULL;
   }

 perror = 0;

 if (cdffile == NULL) perror = 1;
 if (sdffile == NULL) perror = 1;

 if (indemofile == NULL) perror = 1;
 if (outdemofile == NULL) perror = 1;
 }
 }

if (perror == 1) {
printf("error\n\n");
printf("Parameter format: LIPPY -sSCENE.SDF -cCHAR.CDF -oOUTDEMO.DM2 -iINDEMO.DM2\n");
exit(1);
} else {
printf("okay\n");
fflush(stdout);
}

printf("Reading character definition file...");
readdef(cdffile,&character);
printf("num2 %d", character.numemotions);
stream  = fopen( "c:/errorchar.txt", "wt" );

for (v = 0; v < character.numemotions; ++v)
	{
		for (o = 0; o < character.emotion[v].numbase; ++o)
			{
				fprintf( stream, "base: %d\n numbases:%d\n emotion number:%d\n emotion name: %s\n numopen: %d\n", character.emotion[v].base[o], character.emotion[v].numbase, v, character.emotion[v].emotionname, character.emotion[v].numopen);
			}
	}
fclose (stream);


 printf(" okay\nReading scene definition file...");

 readscn(sdffile,&character,skn);

printf(" okay\nMerging demo...");
 merge(indemofile, outdemofile, skn);
 printf(" okay\n");

//TESTING
	stream  = fopen( "c:/error.txt", "wt" );
	for (i = 0; i <= skn->changes; ++i)
	{
		fprintf( stream, "time: %d\n	Skin: %d\n", skn->skin[i].time, skn->skin[i].num);
	}
	fclose (stream);


 free(skn);
}
