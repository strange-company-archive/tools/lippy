// LippyHelpDlg.h : header file
//

#if !defined(AFX_LIPPYHELPDLG_H__18837B46_393B_11D3_8448_002018648504__INCLUDED_)
#define AFX_LIPPYHELPDLG_H__18837B46_393B_11D3_8448_002018648504__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CLippyHelpDlg dialog

class CLippyHelpDlg : public CDialog
{
// Construction
public:
	CLippyHelpDlg(CWnd* pParent = NULL);	// standard constructor

// Those finding the stuff functions. 

	CString Skins[255];

	void GetStrings() const
	{
	int i;
	int str;
	int count = m_count;
	FILE *stream;
	stream  = fopen( "c:/Strings.txt", "wt" );
		for (i = 0; i < count; ++i)
		{
			int left;
			int right;
			int newleft;
			int end = Skins[i].GetLength();
			for( int q = 0; q < end; ++q )
			{
				if (Skins[i].GetAt(q) == '.')
				{
					right = q;
				}
				else if (Skins[i].GetAt(q) == '\\')
				{
					left = newleft;
					newleft = q;
				}
			}
			str = 1312 + i + m_offset;
			fprintf( stream, "configstring {\n	index %d; // playerskin[%d]\n	string \"player\\\\%s/%s\";\n}\n", str, i+m_offset, Skins[i].Mid(left+1, (newleft - (left+1))), Skins[i].Mid(newleft+1, (right - (newleft+1))));	
		}
		fclose (stream);
	}

// Dialog Data
	//{{AFX_DATA(CLippyHelpDlg)
	enum { IDD = IDD_LIPPYHELP_DIALOG };
	CListCtrl	m_ctlFileList;
	int		m_count;
	int		m_offset;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLippyHelpDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CLippyHelpDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnBrowse();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LIPPYHELPDLG_H__18837B46_393B_11D3_8448_002018648504__INCLUDED_)
