// FLY.C (c) James Payne, 1999. 


#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "auxil.h"
#include "dm2.h"
#include "fly.h"
#include "char.h"


int merge(char * indemofile, char * outdemofile, skins *scn)
{
int block = 0;
int curskin;
int skinchange;
int mess;
int pos,opos;
int dif,difr;
int h;
int p;
FILE *stream;
FILE *stream2;
int s1,s2;
int f_num;
int demo_block;
int change;
fpos_t mspos,mepos,ospos,oepos,obpos;
fpos_t fp;
unsigned long l1,l2,siz;
int msize,more;
unsigned char c1,c2;
char buffer[16384];
char str[64];
long frame;
unsigned char ID;
int first_frame = 0;
int entity;
int t = 0;
int i;
int start = 0;


entity = scn->entitynum;
curskin = scn->skin[0].num;
demo = fopen(indemofile,"rb");
newdemo = fopen(outdemofile,"wb+");
stream  = fopen( "c:/errorskin.txt", "wt" );
stream2  = fopen( "c:/errormsg.txt", "wt" );


if ((demo == NULL) || (newdemo == NULL)) {
 printf("file error!\n");
 exit(1);
}

pos = 0;
opos = 0;
l1 = 0;
demo_block = -scn->delay;
first_frame = 0;

// block code, I think. 
while(l1 != -1) {
 block++;
 demo_block++;

 fseek(demo,pos,SEEK_SET);

 fread(&l1,sizeof(l1),1,demo); /* blk header 1 */

 fgetpos(newdemo,&obpos);
 fwrite(&l1,sizeof(l1),1,newdemo); /* blk header 1 */

 pos = pos + 4;

 fseek(demo,pos,SEEK_SET);

 more = 1;
 difr = 0;

// Go through the block message by message.

 while ((more == 1) && (l1 != -1)) {

 change = 0;
 dif = 0;

 fgetpos(demo,&mspos);
 mess = read_message();
 fgetpos(demo,&mepos);


//Frame loop

 if (mess == 0x14) {
 /* frame message */
 fsetpos(demo,&mspos);
 frame = read_frame_num();
// printf(".");
 fsetpos(demo,&mepos);
 if (first_frame == 1) {
  first_frame = 0;
//  demo_block = 0;
  }
 }


 //Slightly odd bit of legacy coding. 
 //if (mess == 0x09) {
 //start = 1;
 //}

 /* lookup demo_block number in skin info list */

 for (i = 0; i < scn->changes; i++)
 {
  if (scn->skin[i].time == demo_block) 
  {
	  curskin = scn->skin[i].num;
  }
 }

//Open Packetentity Loop

 if (mess == 0x12) {

 change = 1;
 /* packetentity message, with correct entitynumber */

 fsetpos(demo,&mspos);
 fgetpos(newdemo,&ospos);
 fread(&ID,sizeof(ID),1,demo);
 fwrite(&ID,sizeof(ID),1,newdemo);

 
rewrite_id0x12(curskin,entity);

 fgetpos(newdemo,&oepos);
 fsetpos(demo,&mepos);


 dif = ((int)oepos - (int)ospos) - ((int)mepos - (int)mspos);
// printf("%i",dif);
 fsetpos(newdemo,&obpos);
 fread(&siz,sizeof(siz),1,newdemo);
 s1 = (int)siz;
 s1 = s1 + dif;
 siz = (unsigned long)s1;
 fsetpos(newdemo,&obpos);
 fwrite(&siz,sizeof(siz),1,newdemo);
 fsetpos(newdemo,&oepos);

}

// Message EndPos and MessageStartPos!

 msize = (int)mepos - (int)mspos;

 if (change == 0) {
 fsetpos(demo,&mspos);
 fread(&buffer,1,msize,demo);
 fwrite(&buffer,1,msize,newdemo);
 fsetpos(demo,&mepos);
 }

 p = (int)mepos;
 if (p>=pos+l1) more = 0;

//End of Message Loop

 }

 fseek(demo,l1+pos,SEEK_SET);
 pos = pos + l1;
//End of block loop.

}

fclose(demo);
fclose(newdemo);

}

