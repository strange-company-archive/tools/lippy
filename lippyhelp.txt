LippyHelp

LippyHelp is an UNSUPPORTED application. It was written for internal use only- if it doesn't work, works wrongly, or destroys the fabric of reality, we disclaim all responsibility. 

Installation

1) Unzip Lippyhelp.exe into your Quake 2 directory.


About LippyHelp

LippyHelp is a utility for adding large numbers of skins to a .dm2 file. It was originally developed to aid Strange Company in the creation of Eschaton: Nightfall Special Edition, a lip synched version of Eschaton: Nightfall.

Using LippyHelp

1) You must first turn your .dm2 file into an ls text file using Uwe Gurlich's LMPC available from http://www.planetquake.com/demospecs/ .

2) Read through the ls file to determine the last skin number used in the demo. Enter this in the "Last Existing Skin" box. If you wish to overwrite the existing skins, leave the value as zero. 

3) Click on the "add" button.

4) Select the skins you wish to add to the demo file. Click on the  "OK" Button.

5) Open the "Strings.txt" file that has been generated on "C:\" and copy the entirety of the contents.

6) Paste the contents of "strings.txt" into the ls file. It should be placed immediatly after all the config strings describing the skins from the original demo, or overwriting the skin config strings if you prefer. 

7)Use LMPC to turn the ls file back into a .dm2 file.


Credits
Lippyhelp lead programer: Hugh Hancock


LippyHelp copyright 1999 Strange Company. 
http://www.strangecompany.org/
email: nomad@strangecompany.org

Memes don't exist- tell your friends. 
