entity 3
emotion normal
{
base 32
blink 36
blinkfreq 15
open 33
open 34
open 35
}
emotion angry
{
base 23
blink 27
blinkfreq 10
open 24
open 25
open 26
}
emotion shouting
{
base 23
blink 27
blinkfreq 10
open 37
open 38
open 39
}
emotion caring
{
base 28
blink 31
blinkfreq 10
open 29
open 30
}
emotion thinking
{
base 41
blink 45
blinkfreq 10
open 42
open 43
open 44
}
emotion smile
{
base 40
}
end
