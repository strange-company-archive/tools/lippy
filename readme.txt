Lippy: The Strange Company Lip Synching Utility


About Lippy

Lippy is a utilty for adding lip synching to quake 2 .dm2 files. It was developed by Strange Company to allow the easy creation of a lip synched version of Eschaton: Nightfall. 


Installation

1) Unzip this .zip into your Quake 2 directory.


Using Lippy: 


1) Create a backup of the .dm2 file you wish to lipsynch!

2) Make sure you have all the skins you wish to use for lipsynching. 

3) If the lip synch skins are not already in your .dm2 file, either add them manually in Kegrip 2 or use the lippyhelp program (see lippyhelp.txt). Then place the .dm2 file in a new directory along with a copy of the lippy files.

Now we need to create the two types of file which Lippy uses to determine the lip-synching in your demo: the Character Definition File (CDF), which defines the talking skins and potential emotions of each character, and the Scene Definition File (SDF) which defines the lip-synching over a timeframe. 

4) Create a character definition file for each of your characters by:

[for all of section 4, refer to the example .cdfs in the lippy distribution .zip]

4a) Open up a new text file in note pad.

4b) Declare which entity this .cdf is for by typing "entity x" where "x" is the entity number for the character.

4c) Declare the name of the emotion being used by typing 
"emotion xxxxxxxx", where "xxxxxxxx" is the emotion name. The emotion must be named, even if there is only one emotion.

4d) On a new line type a "{" to tell the program you are now entering emotion infomation.

4e) On a new line type "base x" where "x" is the closed mouth skin number for this emotion. Multiple closed states may be entered in the same way, taking a new line for each. If multiple closed states are used, Lippy will pick a random closed state each time the entity stops talking.

4f) On a new line type "blink x" where "x" is the eyes closed skin number for this emotion. If blinking is not being used, ignore.

4g) On a new line type "blinkfreq x" where "x" is the avarage number of frames between blinks. If blinking is not being used, ignore.

4h) On a new line type "open x" where "x" is a mouth open skin number for this emotion.  Multiple open states may be entered in the same way, taking a new line for each. If multiple open states are used, Lippy will pick a random open state each time the entity start talking or changes phonym. If this emotion has no open states, ignore.

4i) On a new line type a "}" to tell the program you are no longer entering information for the above emotion.

4j) If another emotion is required, go back to step "4c".

4k) if you have now finished entering emotions, type "END" on a new line.

4l) save the file as "XXXXXXXX.cdf" where "XXXXXXXX" is the file name.


5) Create a scene definition file for each character by

[for all of section 4, refer to the example .sdfs in the lippy distribution .zip]

5a) Open up a new text file in note pad.

5b) Declare which block the sound file starts at by typing 
"start delay x+6", where "x" is the block number of the start of the audio file. So, for a file in which the sound starts at block 10, the start delay is 16.

5c) Declare the starting emotion for the scene by typing "xxxxxx 0", where "xxxxxx" is an emotion in the characters CDF file.

5d) Enter the lip synching information in the following way;

To declare the start of a section of dialogue from the character, use the "{" symbol before the time when the dialogue starts. All times must be given as tenths of a second, e.g. 4 seconds = 40 and should be determined by the audio file and not the .dm2.

To declare a change of phoneme (mouth shape), causing lippy to pick another open skin for this emotion, use the "-" symbol before the time. Lippy will make random phoneme changes at random times during speach unless changes are declared in the sdf. (In the Lippy Tehcnical demo, both Cerise and Poet used the random changes)

To declare the end of a section of dialogue from the character, place the "}" symbol before the time.

Example

{81  start talking 
-93  change phonym
}100 stop talking

There will be a definite phonym change at time 93, the rest will be determined by lippy.

5e) To change emotions, type "xxxxxxxx y" where "xxxxxxx" is the emotion name and "y" is the time at which the emotion change takes place. All times must be given as tenths of a second (4 seconds into the demo would be defined as  "= 40") and should be determined by the audio file and not the .dm2.

5f) when all the lip synching and emotion information has been entered. Type "END" at the end of the file.

5g) Save the file as "xxxxxxxx.sdf" where "xxxxxxxx" is the filename.

6) Repeat steps 4 and 5 for each character in the scene.

7) You should now have a directory with the following, one copy of the Lippy files, a copy of the .dm2 file with all the necessary skins added, .cdf files for every talking character in the scene, .sdf files for every talking character in the scene.

8)you must now create a .bat file for the first character to be lip synched. 

8a) Open a new instance of notepad and type the following.
"LIPPY -cwwwwwwww.CDF -sxxxxxxxx.SDF -iyyyyyyyy.DM2 -ozzzzzzzz.DM2"
Where "wwwwwwww.cdf" is the name of the character definition file for the character to be lip synched. 
Where "xxxxxxxx.cdf" is the name of the script definition file for the character to be lip synched. 
Where "yyyyyyyy.dm2" is the name of the demo file in which the character is to be lip synched.
Where "zzzzzzzz.dm2" is the name of the demo file which Lippy will generate with the skin changes in place.

8b) Save the text file as "xxxxxxxx.bat" where "xxxxxxxx" is  the filename.

9) Run the .bat file you just created.

10) Open the new .dm2 file that has been generated by Lippy in Keygrip 2. 

11) Save the .dm2 file.

12) To lip synch another character, create a new .bat file as above, but this time use the new .dm2 file generated by lippy as the one to be changed.

13) Sit back and watch your lovely lip synched demo.


Things to remember


CDF files

- You must declare an entity at the start of the CDF

- the CDF is based on entity numbers. If an entity changes model you will have to set up emotions for that model as well.

- You must have a base state in every emotion.

- You can only have sixteen skins in each state.

- the first emotion listed will be the initial emotion set in the scene

- file must end with 'END'



SDF

- The SDF works on the entity selected by the CDF, you should include emotion changes to the new models skins when it the entity changes model.

- Make sure all the emotions are defined in the CDF

- If you do not give a start delay it will begin lip synching from block 0

- If you have not declared open states for an emotion then the character cannot talk until it changes emotion.

-Blinking only works when the character is not talking

- If you have not declared blink states and blinkfreq for an emotion then the character will not blink between lines until it changes emotion.

- The file must end with "END"


General

- You must use lippy over various generations in order to lip synch multiple characters in one scene. 

For example

in             	out
scene1.dm2	scene1new.dm2
scene1new.dm2	scene1newer.dm2
scene1newer.dm2	scene1final.dm2

Yhis means that scene1final.dm2 will have three sets of synched characters.

- You must load the synched files into Keygrip 2 and save them before they can be run in quake 2.

- If you get confused, try looking at the examples.

- It's probably easiest if you do the work on each scene in a seperate folder.

Credits

Lippy lead programmer: James Payne
Assistant programmer: Hugh Hancock
Bug fixes: Hugh Hancock
QA: Gordon McDonald

Lippy copyright 1999 Strange Company. 
http://www.strangecompany.org/
email: nomad@strangecompany.org

Memes don't exist- tell your friends.
