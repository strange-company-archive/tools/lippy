// LippyHelpDlg.cpp : implementation file
//

#include "stdafx.h"
#include "LippyHelp.h"
#include "LippyHelpDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// CAboutDlg Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLippyHelpDlg dialog

CLippyHelpDlg::CLippyHelpDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CLippyHelpDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CLippyHelpDlg)
	m_count = 0;
	m_offset = 0;
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CLippyHelpDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLippyHelpDlg)
	DDX_Control(pDX, IDC_LIST1, m_ctlFileList);
	DDX_Text(pDX, IDC_COUNT, m_count);
	DDX_Text(pDX, IDC_OFFSET, m_offset);
	//}}AFX_DATA_MAP
	for (int i = 0; i < m_ctlFileList.GetItemCount(); ++i)
	{
		Skins[i] = m_ctlFileList.GetItemText(i, 0);
	}
}

BEGIN_MESSAGE_MAP(CLippyHelpDlg, CDialog)
	//{{AFX_MSG_MAP(CLippyHelpDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON1, OnBrowse)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLippyHelpDlg message handlers

BOOL CLippyHelpDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CLippyHelpDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CLippyHelpDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CLippyHelpDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CLippyHelpDlg::OnBrowse() 
{
	// This is called in response to the user browsing for skin files. 
	
	// This is called in response to the user browsing for demo files. 
	   char bigBuff[2048] = "";  // maximum common dialog buffer size

   char szFilter[] =      "Skin files (.pcx)|*.pcx|";

   CFileDialog filedlg(true, 
	   NULL, 
	   NULL,
      OFN_HIDEREADONLY | OFN_ALLOWMULTISELECT, szFilter);

   // Modify OPENFILENAME members directly to point to bigBuff
   filedlg.m_ofn.lpstrFile = bigBuff; 
   filedlg.m_ofn.nMaxFile = sizeof(bigBuff);
//   filedlg.DoModal();
   	if (IDOK != filedlg.DoModal())
		return;
	POSITION pos = filedlg.GetStartPosition();
	while (pos)
			m_ctlFileList.InsertItem(m_ctlFileList.GetItemCount(),filedlg.GetNextPathName(pos));
	SetDlgItemInt(IDC_COUNT, m_ctlFileList.GetItemCount());	
}
