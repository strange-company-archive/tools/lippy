// LippyHelp.h : main header file for the LIPPYHELP application
//

#if !defined(AFX_LIPPYHELP_H__18837B44_393B_11D3_8448_002018648504__INCLUDED_)
#define AFX_LIPPYHELP_H__18837B44_393B_11D3_8448_002018648504__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CLippyHelpApp:
// See LippyHelp.cpp for the implementation of this class
//

class CLippyHelpApp : public CWinApp
{
public:
	CLippyHelpApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLippyHelpApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CLippyHelpApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LIPPYHELP_H__18837B44_393B_11D3_8448_002018648504__INCLUDED_)
