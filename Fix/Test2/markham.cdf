emotion normal
{
base 0
half 1
blink 4
blinkfreq 0
open 1
open 2
open 3
}
emotion angry
{
base 5
half 6
blink 9
blinkfreq 0
open 6
open 7
open 8
}
emotion shouting
{
base 5
half 29
blink 9
blinkfreq 0
open 29
open 30
open 31
}
emotion caring
{
base 10
half 11
blink 14
blinkfreq 0
open 11
open 12
open 13
}
emotion horror
{
base 15
half 16
blink 19
blinkfreq 0
open 16
open 17
open 18
}
emotion thinking
{
base 20
half 21
blink 24
blinkfreq 0
open 21
open 22
open 23
}
emotion dark
{
base 25
half 26
open 26
open 27
open 28
}
emotion extrabase
{
base 32
}
emotion smile
{
base 31
}
emotion extrashouting
{
base 38
half 62
blink 42
blinkfreq 0
open 62
open 63
open 64
}