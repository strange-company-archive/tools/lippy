// AUXIL.C (c) James Payne, 1999. 

#include <stdlib.h>
#include <stdio.h>

#include "auxil.h"

unsigned char get_next_unsigned_char()
{
unsigned char a;
fread(&a,sizeof(a),1,demo);
return a;
}

void put_next_unsigned_char(unsigned char a)
{
fwrite(&a,sizeof(a),1,newdemo);
}

signed char get_next_signed_char()
{
signed char a;
fread(&a,sizeof(a),1,demo);
return a;
}

void put_next_signed_char(signed char a)
{
fwrite(&a,sizeof(a),1,newdemo);
}

short get_next_short()
{
short a;
fread(&a,sizeof(a),1,demo);
return a;
}

void put_next_short(short a)
{
fwrite(&a,sizeof(a),1,newdemo);
}

long get_next_long()
{
long a;
fread(&a,sizeof(a),1,demo);
return a;
}

void put_next_long(long a)
{
fwrite(&a,sizeof(a),1,newdemo);
}

float get_next_float()
{
float a;
fread(&a,sizeof(a),1,demo);
return a;
}

void put_next_float(float a)
{
fwrite(&a,sizeof(a),1,newdemo);
}


int ReadChar()
{
return (int) get_next_signed_char();
}

void WriteChar(int a)
{
put_next_signed_char((signed char)a);
}


int ReadByte()
{
return (int) get_next_unsigned_char();
}

void WriteByte(int a)
{
put_next_unsigned_char((unsigned char)a);
}


int ReadShort()
{
return (int) get_next_short();
}

void WriteShort(int a)
{
put_next_short((short)a);
}

int ReadLong()
{
return (int)get_next_long();
}

void WriteLong(long a)
{
put_next_long((long)a);
}


float ReadFloat()
{
return (float)get_next_float();
}


void WriteFloat(float a)
{
put_next_float((float) a);
}



char* ReadString()
{
int i;
char* string_pointer;
char string_buffer[0x800];
string_pointer=string_buffer;
for (i=0 ; i<0x7FF ; i++, string_pointer++) {
    if (! (*string_pointer = ReadChar()) ) break;
    }
*string_pointer = '\0';
return (char *)strdup(string_buffer);
}

vec_t ReadCoord()
{
return (vec_t) (ReadShort() * 0.125);
}

void WriteCoord(vec_t a)
{
WriteShort(a * 8);
}


void ReadPosition(vec_t* pos)
{
int i;
for (i=0 ; i<3 ; i++) pos[i] = ReadCoord();
}

void WritePosition(vec_t* pos)
{
int i;
for (i=0 ; i<3 ; i++) WriteCoord(pos[i]);
}



void ReadDir(vec_t* pos)
{
pos[0] = 0;
pos[1] = 0;
pos[2] = 0;
ReadByte();

/*
#define NUMVERTEXNORMALS 162
int code;
float avertexnormals[NUMVERTEXNORMALS][3] = {
#include "q2source_12_11/utils3/qdata/anorms.h" 
}
code = ReadByte();
if (code >= NUMVERTEXNORMALS) error("MSF_ReadDir: out of range");
pos[0] = avertexnormals[code][0];
pos[1] = avertexnormals[code][1];
pos[2] = avertexnormals[code][2];
*/

}

vec_t ReadAngle()
{
return (vec_t) ReadChar() * 360.0 / 256.0;
}
void WriteAngle(float pos)
{
WriteChar((int)((pos * 256.0) / 360.0));
}
vec_t ReadAngle16()
{
int g;
vec_t h;
g = ReadShort();
h = ((float)g * 360.0) / 65536.0;
return h;
}
void WriteAngle16(float pos)
{
WriteShort((short)((pos*65536.0)/360.0));
}


