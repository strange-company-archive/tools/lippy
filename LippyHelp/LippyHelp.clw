; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CLippyHelpDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "LippyHelp.h"

ClassCount=3
Class1=CLippyHelpApp
Class2=CLippyHelpDlg
Class3=CAboutDlg

ResourceCount=5
Resource1=IDD_ABOUTBOX
Resource2=IDR_MAINFRAME
Resource3=IDD_LIPPYHELP_DIALOG
Resource4=IDD_ABOUTBOX (English (U.S.))
Resource5=IDD_LIPPYHELP_DIALOG (English (U.S.))

[CLS:CLippyHelpApp]
Type=0
HeaderFile=LippyHelp.h
ImplementationFile=LippyHelp.cpp
Filter=N

[CLS:CLippyHelpDlg]
Type=0
HeaderFile=LippyHelpDlg.h
ImplementationFile=LippyHelpDlg.cpp
Filter=D
BaseClass=CDialog
VirtualFilter=dWC
LastObject=IDC_OFFSET

[CLS:CAboutDlg]
Type=0
HeaderFile=LippyHelpDlg.h
ImplementationFile=LippyHelpDlg.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_LIPPYHELP_DIALOG]
Type=1
Class=CLippyHelpDlg
ControlCount=7
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_LIST1,SysListView32,1350631443
Control4=IDC_BUTTON1,button,1342242816
Control5=IDC_COUNT,edit,1350633600
Control6=IDC_OFFSET,edit,1350631552
Control7=IDC_STATIC,static,1342308352

[DLG:IDD_LIPPYHELP_DIALOG (English (U.S.))]
Type=1
Class=?
ControlCount=7
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_LIST1,SysListView32,1350631443
Control4=IDC_BUTTON1,button,1342242816
Control5=IDC_COUNT,edit,1350633600
Control6=IDC_OFFSET,edit,1350631552
Control7=IDC_STATIC,static,1342308352

[DLG:IDD_ABOUTBOX (English (U.S.))]
Type=1
Class=?
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

