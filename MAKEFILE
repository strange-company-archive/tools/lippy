TARGET=DJGPP

DEST=lippy
LFLAGS= -s

CFLAGS=
TARGETFLAGS= -DFASTBITS -DGNUCI486 -Wall -O2 -m486 -fomit-frame-pointer -funroll-all-loops -finline-functions -ffast-math
L=gcc

OBJECTS=inputdef.o main.o inputscn.o auxil.o fly.o dm2.o


$(DEST): $(OBJECTS)
	@echo $@
	@$(L) $(LFLAGS) $(OBJECTS) -o $(DEST)

%.o: %.c
	@echo $@
	@$(L) -c $(CFLAGS) $(CFLAGS) $< -o $@

clean:
	@del *.o
	@del $(DEST).exe
