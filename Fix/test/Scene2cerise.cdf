entity 2

emotion normal
{
base 10
half 10
blink 14
blinkfreq 0
open 11
open 12
open 13
}
emotion angry
{
base 0
half 0
blinkfreq 4
freq 0
open 1
open 2
open 3
}
emotion happy
{
base 5
half 5
blink 9
blinkfreq 0
open 6
open 7
open 8
}
emotion sad
{
base 15
half 15
blink 19
blinkfreq 0
open 16
open 17
open 18
}

end